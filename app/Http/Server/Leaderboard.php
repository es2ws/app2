<?php

namespace App\Http\Server;

use Framework\Application;
use Framework\Connections\Redis;
use Ratchet\ConnectionInterface;
use Ratchet\RFC6455\Messaging\MessageInterface;
use Ratchet\WebSocket\MessageComponentInterface;

class Leaderboard implements MessageComponentInterface
{
	private $app;

	protected $clients;
	protected $redisClient;
	protected $redisChannel = 'highscore_channel';

	/**
	 * Leaderboard constructor.
	 */
	public function __construct( Application $app) {
		$this->app = $app;
		$this->clients = new \SplObjectStorage;

		$this->redisClient = $this->app->getConnections(Redis::class)->initialize()->getClient();
	}

	/**
	 * When a new connection is opened it will be passed to this method
	 *
	 * @param  ConnectionInterface $conn The socket/connection that just connected to your application
	 *
	 * @throws \Exception
	 */
	function onOpen( ConnectionInterface $conn )
	{
		$this->clients->attach($conn);

		echo "New connection! ({$conn->resourceId})\n";

		$this->sendInitialMessage($conn);
	}

	/**
	 * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
	 *
	 * @param  ConnectionInterface $conn The socket/connection that is closing/closed
	 *
	 * @throws \Exception
	 */
	function onClose( ConnectionInterface $conn )
	{
		// The connection is closed, remove it, as we can no longer send it messages
		$this->clients->detach($conn);

		echo "Connection {$conn->resourceId} has disconnected\n";
	}

	/**
	 * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
	 * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
	 *
	 * @param  ConnectionInterface $conn
	 * @param  \Exception $e
	 *
	 * @throws \Exception
	 */
	function onError( ConnectionInterface $conn, \Exception $e )
	{
		echo "An error has occurred: {$e->getMessage()}\n";

		$conn->close();
	}

	/**
	 *
	 * @param ConnectionInterface $conn
	 * @param MessageInterface $msg
	 */
	public function onMessage( ConnectionInterface $conn, MessageInterface $msg )
	{
		$numRecv = count($this->clients) - 1;
		echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
			, $conn->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

		foreach ($this->clients as $client) {
			if ($conn !== $client) {
				// The sender is not the receiver, send to each client connected
				$client->send($msg);
			}
		}
	}

	/**
	 * Getting the current score for players and send it back to connected user
	 *
	 * @param $conn
	 */
	private function sendInitialMessage( $conn){
		$range = $this->redisClient->zrange('highscore',0,-1,array( 'withscores'  => true));
		$conn->send( json_encode($range) );
	}
}