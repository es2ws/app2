<?php

namespace App\Console\Commands;

use Framework\Application;
use Framework\Command;
use App\Http\Server\Leaderboard;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;

class WebSocketServer extends Command
{

	/**
	 * WebSocketServer constructor.
	 *
	 * @param Application $app
	 */
	public function __construct(Application $app)
	{
		$server = IoServer::factory(
			new HttpServer(
				new WsServer(
					new Leaderboard($app)
				)
			),
			8080
		);

		$server->run();
	}

}