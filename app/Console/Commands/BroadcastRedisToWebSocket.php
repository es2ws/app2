<?php

namespace App\Console\Commands;

use Framework\Application;
use Framework\Command;
use Framework\Connections\Redis;
use React\EventLoop\Factory;
use React\Socket\Connector;

class BroadcastRedisToWebSocket extends Command
{

	private $app;

	protected $arguments = [];
	protected $loop;
	protected $redis;
	protected $redisClient;
	protected $redisChannel = 'highscore_channel';
	protected $webSocketSettings;

	/**
	 * BroadcastRedisToWebSocket constructor.
	 *
	 * @param Application $app
	 * @param $arguments
	 */
	public function __construct( Application $app, $arguments )
	{
		$this->app       = $app;
		$this->arguments = $arguments;

		$this->webSocketSettings = (object)[
			'host' => $_ENV['WEBSOCKET_HOST'],
			'port' => $_ENV['WEBSOCKET_PORT']
		];

		$this->loop        = Factory::create();
		$this->redis       = $this->app->getConnections( Redis::class )->initializeAsync( $this->loop );
		$this->redisClient = $this->redis->getClient();

		$this->initializeWebSocket();
		$this->loop->run();
	}

	/**
	 * Initializing Web Socket connection
	 */
	function initializeWebSocket(){
		$host = $this->webSocketSettings->host;
		$port = $this->webSocketSettings->port;

		$reactConnector = new Connector( $this->loop );
		$connector      = new \Ratchet\Client\Connector( $this->loop, $reactConnector );

		$connector( "ws://$host:$port" )->then(
			function ( $conn ) {
				$this->onConnect( $conn );
			}
		);
	}

	/**
	 * Listen when will be connected to web socket and then subscribe to redis channel and send message through websocket
	 *
	 * @param \Ratchet\Client\WebSocket $conn
	 */
	function onConnect( \Ratchet\Client\WebSocket $conn )
	{
		$this->redisClient->subscribe( $this->redisChannel );

		$this->redisClient->on(
			'message', function ( $channel, $payload ) use ( $conn ) {
			$conn->send( $payload );
		}
		);
	}
}