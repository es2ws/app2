#Instructions to use the App2

1. Create .env file
    - Insert following:
    ```
        REDIS_HOST=127.0.0.1
        REDIS_PASSWORD=
        REDIS_PORT=6379
        WEBSOCKET_HOST=localhost
        WEBSOCKET_PORT=8080
    ```
2. Run ```composer install```
3. Create virtual host **pokerstars.app2.local**
4. Run following commands from root:
    - php index.php WebSocketServer - *this command will start the websocket server*
    - php index.php BroadcastRedisToWebSocket - *this command will broadcast redis changes to websocket*