<?php

namespace Routes;

use Framework\Application;
use Framework\Singleton;
use Steampixel\Route;

class Web extends Singleton
{
	protected static $instance;

	public $app = null;

	/**
	 * Web constructor.
	 *
	 * @param Application $app
	 */
	public function __construct( Application $app )
	{
		$this->app = $app;
	}

	/**
	 *
	 */
	public function init()
	{

	}
}